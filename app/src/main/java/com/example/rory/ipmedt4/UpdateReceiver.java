package com.example.rory.ipmedt4;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Jelle Wielinga on 28-1-2015.
 */
public class UpdateReceiver extends BroadcastReceiver
{
    public void onReceive(Context context, Intent intent)
    {
        System.out.println("alarm received");
        Toast.makeText(context, "I'm running", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(Intent.ACTION_SCREEN_OFF);
        i.setClassName("com.example.rory.ipmedt4", "com.example.rory.ipmedt4.UpdateMethod");
        i.addFlags(i.FLAG_ACTIVITY_NEW_TASK|i.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(i);
    }
}
