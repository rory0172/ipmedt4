package com.example.rory.ipmedt4;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Jelle Wielinga on 28-1-2015.
 */
public class UpdateMethod extends Activity
{
    FusedLocationService fusedLocationService;
    String ip = "145.101.92.14";

    protected void onCreate(Bundle savedInstanceState)
    {
         moveTaskToBack(true);
        super.onCreate(savedInstanceState);
        fusedLocationService = new FusedLocationService(this);
        Thread welcome = new Thread()
        {
            public void run()
            {
                try
                {
                    //Dit aantal moet hoger all hij null terugkrijgt
                    sleep(500);
                }

                catch(Exception error)
                {
                    error.printStackTrace();
                }

                finally
                {
                    sendUpdate();
                    finish();
                }
            }
        };
        welcome.start();

        finish();
    }

    public void sendUpdate()
    {
        Location location = fusedLocationService.getLocation();
        System.out.println("UPDATE METHOD STARTED, LOCATION GET: " + location);

        if (null != location)
        {
            fusedLocationService = new FusedLocationService(this);
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            JSONObject fullData = new JSONObject();
            JSONObject data = new JSONObject();
            JSONArray dataArray = new JSONArray();

            try
            {
                data.put("id", 55);
            } catch (JSONException e) {}

            try
            {
                data.put("latitude", latitude);
            } catch (JSONException e) {}

            try
            {
                data.put("longitude", longitude);
            } catch (JSONException e) {}

            dataArray.put(data);

            try
            {
                fullData.put("update", dataArray);
                System.out.println("console: attempt connection, data send: " + dataArray);
                new ServerCommunicator(this, ip, 4444, fullData.toString()).execute();
            } catch (JSONException e) {e.printStackTrace();}
        }

        else
        {
            System.out.println("Update method failed to update");
        }
    }
}
