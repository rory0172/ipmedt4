package com.example.rory.ipmedt4;

import android.app.Activity;

/**
 * Created by Jelle Wielinga on 18-12-2014.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import java.util.Calendar;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Hoofdscherm extends Activity
{
    public PendingIntent pendingIntent;
    public MainActivity mainActivity;
    ImageButton alarmButton;
    Button cancelButton;
    public String alarmState = "0";
    Animation outAnimation;
    Animation inAnimation;
    ImageView fadeView;
    private static final String TAG = "Hoofdscherm";
    Button btnFusedLocation;
    TextView tvLocation;
    TextView tvAlarmState;
    FusedLocationService fusedLocationService;
    String ip;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        System.out.println("Hoofdscherm started");
        ip = "145.101.92.14";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hoofdscherm);

        outAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadeout);
        inAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        tvAlarmState = (TextView) findViewById(R.id.tvAlarmState);

        fusedLocationService = new FusedLocationService(this);
        addListenerAlarmButton();
        addListenerCancelButton();
        addListenerLocationTestButton();
        Intent alarmIntent = new Intent(Hoofdscherm.this, UpdateReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(Hoofdscherm.this, 0, alarmIntent, 0);

        Thread firstTime = new Thread()
        {
            public void run()
            {
                try
                {
                    sleep(500);
                }

                catch(Exception error)
                {
                    error.printStackTrace();
                }

                finally
                {
                    firstTimeSend();
                }
            }
        };
        firstTime.start();
        updateManager();
    }

    public void addListenerLocationTestButton()
    {
        fusedLocationService = new FusedLocationService(this);
        btnFusedLocation = (Button) findViewById(R.id.btnGPSShowLocation);
        btnFusedLocation.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View arg0)
            {Location location = fusedLocationService.getLocation();
                String locationResult = "";
                if (null != location)
                {
                    Log.i(TAG, location.toString());
                    System.out.println( System.currentTimeMillis());
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
                    float accuracy = location.getAccuracy();
                String provider = location.getProvider();
                double altitude = location.getAltitude();
                locationResult = "Latitude: " + latitude + "\n" +
                        "Longitude: " + longitude + "\n" +
                        "Altitude: " + altitude + "\n" +
                        "Accuracy: " + accuracy + "\n" +
                        "Provider: " + provider + "\n";
            } else {
                locationResult = "Location Not Available!";
            }
            tvLocation.setText(locationResult);
        }
        });
    }

    public void addListenerAlarmButton()
    {
        fusedLocationService = new FusedLocationService(this);
        alarmButton = (ImageButton) findViewById(R.id.alarmButton);
        fadeView = (ImageView) findViewById(R.id.fadeImage);

        alarmButton.setOnClickListener(new OnClickListener()
        {
            public void onClick(View arg0)
            {
                if (alarmState == "0")
                {
                    System.out.println("listener werkt");
                    Toast toast = Toast.makeText(Hoofdscherm.this, "Alarm scherpgesteld", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    cancel();

                    fadeView.setImageResource(R.drawable.button_green);
                    alarmButton.setImageResource(R.drawable.button_orange);
                    alarmButton.startAnimation(inAnimation);

                    cancelButton.setEnabled(true);
                    alarmState = "1";
                    tvAlarmState.setText(alarmState);
                }
                else if (alarmState == "1")
                {
                    Toast toast = Toast.makeText(Hoofdscherm.this, "Alarm geactiveerd", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    sendAlarm();

                    fadeView.setImageResource(R.drawable.button_orange);
                    alarmButton.setImageResource(R.drawable.button_red);
                    alarmButton.startAnimation(inAnimation);

                    alarmButton.setImageResource(R.drawable.button_red);
                    cancelButton.setEnabled(true);
                    alarmState = "2";
                    tvAlarmState.setText(alarmState);
                }
            }
        });
    }

    public void addListenerCancelButton()
    {
        cancelButton = (Button) findViewById(R.id.cancelButton);

        cancelButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                if (alarmState == "1") {
                    Toast toast = Toast.makeText(Hoofdscherm.this, "Alarm gecancelled", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    fadeView.setImageResource(R.drawable.button_orange);
                    alarmButton.setImageResource(R.drawable.button_green);
                    alarmButton.startAnimation(inAnimation);
                    updateManager();
                    cancelButton.setEnabled(false);
                    alarmState = "0";
                    tvAlarmState.setText(alarmState);
                } else if (alarmState == "2") {
                    Toast toast = Toast.makeText(Hoofdscherm.this, "Alarm gedeactiveerd", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    fadeView.setImageResource(R.drawable.button_red);
                    alarmButton.setImageResource(R.drawable.button_green);
                    alarmButton.startAnimation(inAnimation);
                    updateManager();
                    cancelButton.setEnabled(false);
                    alarmState = "0";
                    tvAlarmState.setText(alarmState);
                }
            }
        });
    }

    public void sendAlarm()
    {
        Location location = fusedLocationService.getLocation();
        System.out.println("ALARM METHOD STARTED, LOCATION GET: " + location);

        if (null != location)
        {
            fusedLocationService = new FusedLocationService(this);
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            JSONObject fullData = new JSONObject();
            JSONObject data = new JSONObject();
            JSONArray dataArray = new JSONArray();

            try
            {
                data.put("id", 55);
            } catch (JSONException e) {}

            try
            {
                data.put("latitude", latitude);
            } catch (JSONException e) {}

            try
            {
                data.put("longitude", longitude);
            } catch (JSONException e) {}

            dataArray.put(data);

            try
            {
                fullData.put("alarm", dataArray);
                System.out.println("console: attempt connection, data send: " + dataArray);
                new ServerCommunicator(this, ip, 4444, fullData.toString()).execute();
            } catch (JSONException e) {e.printStackTrace();}
        }

        else
        {
            System.out.println("Update method failed to update");
        }
    }

    public void updateLocation()
    {
        Location location = fusedLocationService.getLocation();
        System.out.println("UPDATE METHOD STARTED, LOCATION GET: " + location);

        if (null != location)
        {
            fusedLocationService = new FusedLocationService(this);
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            JSONObject fullData = new JSONObject();
            JSONObject data = new JSONObject();
            JSONArray dataArray = new JSONArray();

            try
            {
                data.put("id", 55);
            } catch (JSONException e) {}

            try
            {
                data.put("latitude", latitude);
            } catch (JSONException e) {}

            try
            {
                data.put("longitude", longitude);
            } catch (JSONException e) {}

            dataArray.put(data);

            try
            {
                fullData.put("update", dataArray);
                System.out.println("console: attempt connection, data send: " + dataArray);
                new ServerCommunicator(this, ip, 4444, fullData.toString()).execute();
            } catch (JSONException e) {e.printStackTrace();}
        }

        else
        {
            System.out.println("Update method failed to update");
        }
    }

    public void updateManager()
    {

        System.out.println("updateManager called");
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        int interval = 1000 * 5;

        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        Toast.makeText(this, "Alarm Set", Toast.LENGTH_SHORT).show();
    }

    public void cancel()
    {
        System.out.println("updates cancelled called");
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        Toast.makeText(this, "Alarm Canceled", Toast.LENGTH_SHORT).show();
    }

    public void firstTimeSend()
    {
        Location location = fusedLocationService.getLocation();
        System.out.println("FIRST_TIME METHOD STARTED, LOCATION GET: " + location);

        if (null != location)
        {
            fusedLocationService = new FusedLocationService(this);
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            JSONObject fullData = new JSONObject();
            JSONObject data = new JSONObject();
            JSONArray dataArray = new JSONArray();

            try
            {
                data.put("id", 55);
            } catch (JSONException e) {}

            try
            {
                data.put("latitude", latitude);
            } catch (JSONException e) {}

            try
            {
                data.put("longitude", longitude);
            } catch (JSONException e) {}

            dataArray.put(data);

            try
            {
                fullData.put("registreren", dataArray);
                System.out.println("console: attempt connection, data send: " + dataArray);
                new ServerCommunicator(this, ip, 4444, fullData.toString()).execute();
            } catch (JSONException e) {e.printStackTrace();}
        }

        else
        {
            System.out.println("Update method failed to update");
        }
    }

    private boolean isGooglePlayServicesAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
        {
            return true;
        }
        else
        {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }
}



