package com.example.rory.ipmedt4;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity
{
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    /*
    public void alertPeople(View view)
    {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread welcome = new Thread()
        {
            public void run()
            {
                try
                {
                    sleep(2000);
                }

                catch(Exception error)
                {
                    error.printStackTrace();
                }

                finally
                {
                    System.out.println("MainActivity: Hoofdscherm word gestart");
                    startActivity(new Intent(getApplicationContext(), Hoofdscherm.class));
                    System.out.println("MainActivity: Hoofdscherm gestart");
                    finish();
                }
            }
        };

        welcome.start();
    }

}
